# Krótka historia siatki: CCS Grid Layout

Prezentacja nt. Grid Layout wygłoszona na Konferencji Britenet 2019.

Prezentacja pokazuje historię tzw. Holy Grail of Layout, który w różynych epokach był konstruowany na różne sposoby. Źródła HTML/CSS pokazują quirki i hacki jakich trzeba było używać, aby osiągnąć zamierzony efekt. Ku uciesze wszystkich, obecnie mamy dostęp do narzędzia, które pozwala w prosty i czytelny sposób uzyskać naszego "Świętego Graala" - Grid Layout.

---

`Prezentację przygotował i wygłosił: Piotr Wegner`